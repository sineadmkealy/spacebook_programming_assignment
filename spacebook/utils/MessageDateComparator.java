package utils;

import java.util.Comparator;
import java.util.Date;

import models.Message;
import models.User;

public class MessageDateComparator implements Comparator<Message>
{
  @Override
  public int compare(Message a, Message b)
  {
	//TODO: Task 2: Complete implementation of method MessageDateComparator.compare
    //compare the time-date attributes of each message
    //use the Date compareTo method
    //use appropriate attribute of Message b as the parameter - the postedAt field provided.
	  
		Date aDate = a.from.postedAt ; //compare the time-date attributes of first message object
		Date bDate = b.from.postedAt ; //against second message object
		return aDate.compareTo(bDate);

  }
}

